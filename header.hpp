

#include <iostream>
#include <cstring>
#include <algorithm>
#include <cmath>
#include <sstream>
#include <iomanip>
#include <stdint.h>

class RC6{

private:

public:
  // Declarations
  unsigned int w, r, b, log_w;   
                                 

  int64_t modulo;        
  std::string mode, text, key;    
                                 
                                  
  unsigned int *S;                
                                
                                 
  unsigned int *L;

  // RC6 Functons


  void rc_constraints(unsigned int &, unsigned int &);

  void generate_key_schedule(std::string key);

  std::string encrypt(const std::string &);

  std::string decrypt(const std::string &);
 
  std::string convert_to_little_endian(std::string);


  // RC6-w/r/b
  RC6(unsigned int B);

  
  ~RC6();
};


