#include <iostream>
#include <fstream>
#include <cstring>
#include <algorithm>


using namespace std;

#include "header.hpp"

int main(int argc, char *argv[])
{
  fstream inputfile;
  fstream outputfile;
  int mode;
  string text;
  string userKey;
  string read;
  string encrypt = "Encryption";
  string decrypt = "Decryption";
  string plain = "plaintext: ";
  int plainLen = 11;
  string cipher = "ciphertext: ";
  int cipherLen = 12;
  string userKeytext = "userkey: ";
  int keyLen = 10;
  inputfile.open(argv[1], fstream::in);
  
  outputfile.open(argv[2], fstream::out | fstream::trunc);
  
  //parse the input file
  
     getline(inputfile, read);
	  //figure out if encrtpy or decrpt
	  if(read.compare(encrypt) == 0)
	  {
		  //encrpty
		  mode = 0;
		  getline(inputfile, read);
		  if(read.compare(0,plainLen,plain) == 0)
		  {
				
			  text = read.substr(plainLen,read.length()-plainLen);
			  getline(inputfile, read);
			  //get the key
			  userKey = read.substr(keyLen, read.length() - keyLen);
		  }
		  else
		  {
			  text = read.substr(cipherLen,read.length()-cipherLen);
			  getline(inputfile, read);
			  userKey = read.substr(keyLen, read.length() - keyLen);
		  }
	  }
	  //decrpty
	  else
	  {
		  mode = 1;
		  getline(inputfile, read);
		  if(read.compare(0,plainLen,plain) == 0)
		  {
			  text = read.substr(plainLen,read.length()-plainLen);
			  getline(inputfile, read);
			  //get the key
			  userKey = read.substr(keyLen, read.length() - keyLen);
		  }
		  else
		  {
			  text = read.substr(cipherLen,read.length()-cipherLen);
			  getline(inputfile, read);
			  userKey = read.substr(keyLen, read.length() - keyLen);
		  }
	  }
	  cout<<"mode"<<mode<<endl;
  cout<<"text"<<text<<endl;
  cout<<"key"<<userKey<<endl;
int textLen = text.length();
int userKeyLen = userKey.length();
  for(int i = 0; i< textLen;i++)
 {
	if(text[i] == ' ')
	{
		text.erase(i,1);
	}
 }
for(int i = 0; i< userKeyLen;i++)
 {
	if(userKey[i] == ' ')
	{
		userKey.erase(i,1);
	}
 }
  
    
  string result;

  RC6 *rc6 = new RC6((userKey.length() / 2));
 
  
  rc6->generate_key_schedule(userKey);
  
  if(mode == 0)
  {
    string encryption = rc6->encrypt(text);
	string::iterator it = encryption.begin();
    while(it != encryption.end())
	{
      result.push_back(*it);
      result.push_back(*(it+1));
      result = result + " ";
	  it = it + 2;
    }
  }
  else 
  {
    string decryption = rc6->decrypt(text);
	string::iterator it = decryption.begin();
    while(it != decryption.end())
	{
      result.push_back(*it);
      result.push_back(*(it+1));
      result = result + " ";
	  it = it + 2;
    }
  }

  if(mode == 0)
  {
    outputfile << "ciphertext: " << result << endl;
    cout << "ciphertext: " << result << endl;
  }
  else
  {
    outputfile << "plaintext: " << result << endl;
    cout << "plaintext: " << result << endl; 
  }

  delete rc6;
  inputfile.close();
  outputfile.close();

  return 0;
}

RC6::RC6(unsigned int B)
{
  w = 32;
  r = 20;
  b = B;
  log_w = (unsigned int)log2(w);
  //2^32
  int num =2;
  for (int i = 1; i < w; i++) 
	{ 
		num *= 2; 
	} 
  modulo = pow(2, w);
  S = new unsigned int[ 2 * r+4];
}
void RC6::generate_key_schedule(string key)
{
  const unsigned int w_bytes = ceil((float)w / 8);
  const unsigned int c = ceil((float)b / w_bytes);

  unsigned int p, q;
  rc_constraints(p, q);

  L = new unsigned int[c];

	  int i = 0;
	  while(i < c)
  {
    L[i] = strtoul(convert_to_little_endian(key.substr(w_bytes * 2 * i, w_bytes * 2)).c_str(), NULL, 16);
	i++;
  }  

  S[0] = p;

	  i = 1;
	  while(i <= (2 * r + 3))
  {
    S[i] = (S[i - 1] + q) % modulo;
	i++;
  }

  unsigned int A = 0, B = 0, j = 0, s = 0;
  i = 0;
  int v = 3 * max(c, (2 * r + 4));
  unsigned int temp,temp2;
 
	 
      i = 1;
	  while(i <= v)
  {
    
	temp = (S[s] + A + B) % modulo;
	temp2 = 3;
	temp2 <<= w - log_w;
    temp2 >>= w - log_w;
    A = S[s] =  (temp << temp2) | (temp >> (w - temp2));
    
	temp = (L[j] + A + B) % modulo;
	temp2 = A + B;
	temp2 <<= w - log_w;
    temp2 >>= w - log_w;
    B = L[j] =  (temp << temp2) | (temp >> (w - temp2));
    j = (j + 1) % c;
	s = (s + 1) % (2 * r + 4);
	i++;
  }
}
void RC6::rc_constraints(unsigned int &p, unsigned int &q)
{
  q = (unsigned int)((1.618033988749895 - 1) * modulo);
  p = (unsigned int)ceil(((M_E - 2) * modulo));
}


string RC6::convert_to_little_endian(string str_endian)
{
  string lil_endian;
  
  if(str_endian.length() % 2 != 0)
  {
    str_endian = "0" + str_endian;
	string::reverse_iterator r_it = str_endian.rbegin();
    while(r_it != str_endian.rend())
	{
      lil_endian.push_back(*(r_it+1));
      lil_endian.push_back(*r_it);
	  r_it = r_it + 2;
    }
  }
  else
  {
		 string::reverse_iterator r_it = str_endian.rbegin();
		 while(r_it != str_endian.rend())
		 {
      lil_endian.push_back(*(r_it+1));
      lil_endian.push_back(*r_it);
	  r_it = r_it + 2;
    }
    
  }

  return lil_endian;
}





string RC6::encrypt(const string &text)
{
  string result;
  
  unsigned int A, B, C, D;
  A = strtoul(convert_to_little_endian(text.substr(0, 8)).c_str(), NULL, 16);
  B = strtoul(convert_to_little_endian(text.substr(8, 8)).c_str(), NULL, 16);
  C = strtoul(convert_to_little_endian(text.substr(16, 8)).c_str(), NULL, 16);
  D = strtoul(convert_to_little_endian(text.substr(24, 8)).c_str(), NULL, 16);

  unsigned int t, u, temp_A;

  B += S[0];
  D += S[1];
  unsigned int temp,temp2;
  
	 int i = 1; 
	  while(i <= r)
  {
    
	temp = (B * (2 * B + 1)) % modulo;
	temp2 = log_w;
	temp2 <<= w - log_w;
    temp2 >>= w - log_w;
    t =  (temp << temp2) | (temp >> (w - temp2));
	
    
	temp = (D * (2 * D + 1)) % modulo;
	temp2 = log_w;
	temp2 <<= w - log_w;
    temp2 >>= w - log_w;
    u = (temp << temp2) | (temp >> (w - temp2));
	
    
	temp = A ^ t;
	temp2 = u;
	temp2 <<= w - log_w;
    temp2 >>= w - log_w;
    A = ((temp << temp2) | (temp >> (w - temp2)))+ S[2 * i];
	
   
    temp = C ^ u;
	temp2 = t;
	temp2 <<= w - log_w;
    temp2 >>= w - log_w;
    C = ((temp << temp2) | (temp >> (w - temp2)))+ S[2 * i + 1];
	
    temp_A = A;
    A = B;
    B = C;
    C = D;
    D = temp_A;
	i++;
  }

  A += S[2 * r + 2];
  C += S[2 * r + 3];

  
  string str_A, str_B, str_C, str_D;

  stringstream str_stream;
  str_stream << setfill('0') << setw(4) <<hex << A;
  str_A = convert_to_little_endian(str_stream.str());
  str_stream.str("");
  str_stream.clear();

  str_stream << setfill('0') << setw(4) <<hex << B;
  str_B = convert_to_little_endian(str_stream.str());
  str_stream.str("");
  str_stream.clear();
  
  str_stream << setfill('0') << setw(4) <<hex << C;
  str_C = convert_to_little_endian(str_stream.str());
  str_stream.str("");
  str_stream.clear();

  str_stream << setfill('0') << setw(4) <<hex << D;
  str_D = convert_to_little_endian(str_stream.str());
  
  result = str_A + str_B + str_C + str_D;

  return result;
}


string RC6::decrypt(const string &text)
{
  string result;
  
  unsigned int A, B, C, D;
  A = strtoul(convert_to_little_endian(text.substr(0, 8)).c_str(), NULL, 16);
  B = strtoul(convert_to_little_endian(text.substr(8, 8)).c_str(), NULL, 16);
  C = strtoul(convert_to_little_endian(text.substr(16, 8)).c_str(), NULL, 16);
  D = strtoul(convert_to_little_endian(text.substr(24, 8)).c_str(), NULL, 16);

  unsigned int t, u, temp_A;  
  unsigned int temp,temp2;
  C -= S[2 * r + 3];
  A -= S[2 * r + 2];
 
	  int i = r;
	while(i >= 1)
  {
    temp_A = D;
    D = C;
    C = B;
    B = A;
    A = temp_A;
   
	temp = (D * (2 * D + 1)) % modulo;
	temp2 = log_w;
	temp2 <<= w - log_w;
    temp2 >>= w - log_w;
    u = (temp << temp2) | (temp >> (w - temp2));
    
	
	temp = (B * (2 * B + 1)) % modulo;
	temp2 = log_w;
	temp2 <<= w - log_w;
    temp2 >>= w - log_w;
    t = (temp << temp2) | (temp >> (w - temp2));
	
  
	temp = (C - S[2 * i + 1]) % modulo;
	temp2 = t;
	temp2 <<= w - log_w;
    temp2 >>= w - log_w;
    C = ((temp >> temp2) | (temp << (w - temp2))) ^ u;
   
	temp = (A - S[2 * i]) % modulo;
	temp2 = u;
	temp2 <<= w - log_w;
    temp2 >>= w - log_w;
    A = ((temp >> temp2) | (temp << (w - temp2))) ^ t;
	i--;
  }
  D -= S[1];
  B -= S[0];

  string str_A, str_B, str_C, str_D;

  stringstream str_stream;
  str_stream << setfill('0') << setw(4) <<hex << A;
  str_A = convert_to_little_endian(str_stream.str());
  str_stream.str("");
  str_stream.clear();

  str_stream << setfill('0') << setw(4) <<hex << B;
  str_B = convert_to_little_endian(str_stream.str());
  str_stream.str("");
  str_stream.clear();
  
  str_stream << setfill('0') << setw(4) <<hex << C;
  str_C = convert_to_little_endian(str_stream.str());
  str_stream.str("");
  str_stream.clear();

  str_stream << setfill('0') << setw(4) <<hex << D;
  str_D = convert_to_little_endian(str_stream.str());
  
  result = str_A + str_B + str_C + str_D;

  return result;
}

RC6::~RC6()
{
  delete L;
  delete S;
}
